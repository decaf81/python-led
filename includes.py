import time
import random
import struct
from threading import Thread
from neopixel import *
import argparse
import sys
#1 .. 3
#60 .. 62
#119 .. 121
#178 .. 180
#237 .. 239
fullchr = [1,2,3,60,61,62,119,120,121,178,179,180,237,238,239]
letter_a = [2,60,62,119,120,121,178,180,237,239]
letter_c = [1,2,3,60,119,178,237,238,239]
letter_d = [1,2,60,62,119,121,178,180,237,238]
letter_e = [1,2,3,60,119,120,178,237,238,239]
letter_f = [1,2,3,60,119,120,121,178,237]
letter_g = [1,2,3,60,62,119,120,121,180,237,238,239]
letter_h = [1,3,60,62,119,120,121,178,180,237,239]
letter_i = [2,61,120,179,238]
letter_j = [3,62,121,180,239,238,178]
letter_k = [1,60,119,178,237,120,3,239]
letter_l = [1,60,119,178,237,238,239]
letter_m = [1,61,62,3,60,119,178,237,121,180,239]
letter_n = [1,2,3,60,62,119,121,178,180,237,239]
letter_o = [1,2,3,60,62,119,121,178,180,237,238,239]
letter_p = [1,2,3,60,62,119,120,121,178,237]
letter_q = [1,2,3,60,62,119,120,121,180,239]
letter_r = [1,2,3,60,62,119,120,121,178,179,237,239]
letter_s = [1,2,3,60,119,120,121,180,237,238,239]
letter_t = [2,61,120,179,238,1,3]
letter_u = [1,3,60,62,119,121,178,180,237,238,239]
letter_v = [1,3,60,62,119,121,178,180,238]
letter_x = [1,3,61,120,179,237,239]
letter_y = [1,3,60,62,120,179,238]
letter_z = [1,2,3,62,120,178,237,238,239]
rightarrow = [1,61,121,179,237,2,62,122,180,238]
a_hartje = [1,3,60,61,62,20]
bg1 = 0
bg2 = 0
bg3 = 0
charBuffer=[]
PIXELS     = 8
TIME_ALARM = 25
TIME_SLOW  = 500000
US_TO_MS = 1000
LEDS = 330
WIDTH = 59
ROWS = 5
TOTALLEDS = WIDTH * ROWS
stopstars = 0
command='nil'
starsEnabled = 0
# LED strip configuration:
LED_COUNT      = 330      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

def hex2rgb(rgb):
        return struct.unpack('BBB', rgb.decode('hex'))

def bufferString(string,pos):
    process_dot={}
    for i, c in enumerate(string):
        pos = pos + 4

        exec("process_dot=letter_%s" % (c))
        for x in process_dot:
            x = x + pos
            charBuffer.append(x)

def bufferPrint(r,g,b):
    for x in charBuffer:
      strip.setPixelColor(x,Color(r,g,b))
    strip.show()

def bufferClear():
    global charBuffer
    for x in charBuffer:
      strip.setPixelColor(x,Color(bg1, bg2, bg3))
    strip.show()
    charBuffer=[]


def bufferFadeIn(r,g,b):
    for x in charBuffer:
      strip.setPixelColor(x,Color(255,255,255))
      time.sleep(1/10000.0)
      strip.show()
      strip.setPixelColor(x,Color(r,g,b))
      strip.show()

def clearStrip():
    for x in range(strip.numPixels()):
        strip.setPixelColor(x, Color(bg1, bg2, bg3))
        strip.show()

def flash(times): 
    for m in range(times):
        for x in range(strip.numPixels()):
            if x not in charBuffer:
                strip.setPixelColor(x, Color(255,255,255))
        strip.show()
        time.sleep(1/100.0)
        for x in range(strip.numPixels()):
            if x not in charBuffer:
                strip.setPixelColor(x, Color(bg1, bg2, bg3))
        strip.show()
        time.sleep(1/100.0)

def starblink(dot):
    if dot not in charBuffer:
        strip.setPixelColor(dot, Color(0,0,255))
        strip.show()
        time.sleep(1/10000.0)
        strip.setPixelColor(dot, Color(bg1, bg2, bg3))
        strip.show()
    
def stars():
    while True:
        if stopstars == 1:
            break
        
        dot1 = random.randint(1,LED_COUNT)
        dot2 = random.randint(1,LED_COUNT)
        dot3 = random.randint(1,LED_COUNT)
        starblink(dot1)
        starblink(dot2)
        starblink(dot3)

def startStars():
   global starsEnabled
   global stopstars

   if starsEnabled == 0:
        stopstars = 0
        starsEnabled = 1
        print ("Stars not enabled, enabling")
        t = Thread(target=stars)
        t.start()
   elif starsEnabled == 1:
       print ("Stars enabled, disabling")
       starsEnabled = 0
       stopstars = 1


def vorm(type,pos,r,g,b):
    tovorm=[]
    exec ("tovorm=a_%s" % (type))
    for d in tovorm:
            d = d + pos
            strip.setPixelColor(d, Color(r,g,b))
    strip.show()



def processWebCommand(com,arg1,arg2,arg3,arg4,arg5):
    print (com)
    if com == "swipeArrow":
        print "SwipeArrow Called"
        swipeArrow(int(arg1),int(arg2),int(arg3),int(arg4),int(arg5))
        print (arg1)
        print (arg2)
    if com == "Stars":
            print "Stars toggle called"
            startStars()
    if com == "Flash":
            flash(int(arg1))
    if com == "Sem":
            color=hex2rgb(arg1)
            print color
            print "Sem function called"
            bufferString("sem",20)
            print color;
            r = color[1];
            g = color[0];
            b = color[2];
            bufferFadeIn(r,g,b)
    if com == "bufferClear":
            bufferClear()
    if com == "wholestrip":
            setWholecolor(arg1)





def swipeArrow(begin_pos,end_pos,r,g,b):
    swipe=end_pos-begin_pos
    for y in range(begin_pos, end_pos):
        for x in rightarrow:
            d = x + y
            strip.setPixelColor(d, Color(r, g, b))
        strip.show()
        #time.sleep(1/10000.0)

        for x in rightarrow:
            d = x + y 
            strip.setPixelColor(d, Color(bg1, bg2, bg3))

#def setWholeColor(arg):
#    color = hex2rgb(arg)
#    for x in TOTALLEDS:
#        strip.setPixelColor(x, Color(color[1], color[0], color[2])
