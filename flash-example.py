from flask import Flask, request, render_template #import main Flask class and request object

app = Flask(__name__) #create the Flask app

@app.route('/query-example')
def query_example():
        language = request.args.get('language') #if key doesn't exist, returns None
        return render_template('hello.html')

@app.route('/form-example')
def form_example():
             return '''<form method="POST">
                           Language: <input type="text" name="language"><br>
                           Framework: <input type="text" name="framework"><br>
                           <input type="submit" name="submit" value="Submit"><br>
                           </form>'''

@app.route('/json-example')
def json_example():
    return 'Todo...'

if __name__ == '__main__':
        app.run(debug=True, port=5000, host='0.0.0.0')
